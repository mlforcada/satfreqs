# FM satellite frequencies

A CHIRP-compatible CSV file containing the frequencies of some popular FM satellites. It assumes a handheld with 5 kHz steps.

For each satellite, 5 frequencies are provided as a rudimentary Doppler correction: AoS (acquisition of signal), approximation, apex, departure, and LoS (loss of signal). They are marked -2, -1, no mark, +1 and +2 respectively.

For SO-50, the SO50ON frequency is used to turn the satellite on (transmit this for a few seconds if it is quiet).

For the ISS, the frequencies of the crew radio are given (in this case, only three Doppler corrections), but crew often disconnect
the main V/u FM transponder and use it when they have their schduled contacts with ground stations.

Many satellites are only turned on on certain dates. 

If you plan to use the data manually, please be advised that the contents of fields `rToneFreq`, `cToneFreq`, `DtcsCode`, `DtcsPolarity`, `TStep`,`Skip`,`Comment`,`URCALL`,`RPT1CALL`,`RPT2CALL`, and `DVCODE`are irrelevant.

On 5-kHz-step radios, some operators do not apply any Doppler correction to 2-metre-band frequencies, arguing that the maximum Doppler shift is only ±3.5 kHz; this table establishes 2-metre offsets of ±5 kHz for AoS and LoS, as (a) 5 is closer than 0 to 3.5, and (b) for high elevation angles, frequencies at the beginning and the end of the pass vary very slowly. Note that Some Chinese-brand entry-level handhelds have 2.5-kHz steps, but having, say, 7 memories per satellite would easily fill their memory.
